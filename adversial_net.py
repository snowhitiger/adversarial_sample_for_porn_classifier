import os
import cv2
import keras
import numpy as np
import keras.backend as K
from keras.layers import Layer, Input
from keras.models import Model
from keras.callbacks import TensorBoard, ModelCheckpoint


def load_images(image_paths, image_size):
    '''
    Function for loading images into numpy arrays for passing to model.predict
    inputs:
        image_paths: list of image paths to load
        image_size: size into which images should be resized
    
    outputs:
        loaded_images: loaded images on which keras model can run predictions
        loaded_image_indexes: paths of images which the function is able to process
    
    '''
    loaded_images = []
    loaded_image_paths = []

    for i, img_path in enumerate(image_paths):
        try:
            image = keras.preprocessing.image.load_img(img_path, target_size = image_size)
            image = keras.preprocessing.image.img_to_array(image)
            image /= 255
            loaded_images.append(image)
            loaded_image_paths.append(img_path)
        except Exception as ex:
            print(i, img_path, ex)
    
    return np.asarray(loaded_images), loaded_image_paths


class AddNoise(Layer):
    '''Customized layer to add random noise r to the input image x,
    to perturb x to x'=x+r;
    The noise bias r will be learned by minimizing |f(x') - y'|,
    where f() is the pretrained image classifier and y' is the target
    category'''
    def __init__(self, output_dim=None, **kwargs):
        self.output_dim = output_dim
        super(AddNoise, self).__init__(**kwargs)

    def build(self, input_shape):
        # Create a trainable noise bias variable for this layer.
        self.bias = self.add_weight(name='bias', 
                                      shape=input_shape[1:],
                                      initializer='zeros',
                                      trainable=True)
        # Be sure to call this at the end
        super(AddNoise, self).build(input_shape)  

    def call(self, x):
        return K.bias_add(x, self.bias)

    def compute_output_shape(self, input_shape):
        return input_shape



def adversarial_net(model, input_img_path, y_prime=(0, 0, 0, 1, 0),
        categories = ['drawings', 'hentai', 'neutral', 'porn', 'sexy'],
                    img_size=(299, 299),  plot=False):
    '''this function create one adversarial sample of the pretrained image classifier f(x)
    for one given image x, such that a perturbation on x -> x'=x+r can fool f(x') to give
    the wrong label. The loss function is defined as,
        L(x, r, y', c) = |f(x'=x+r) - y'| + c|r|
    Parameters:
        model: keras Model
            pretrained image classifer 
        y_prime: the onehot category array
            reference value for the target category
    Returns:
        input_img, (adversarial_img, f(x')) '''
    input_img = Input(shape=(img_size[0], img_size[1], 3))
    xprime = AddNoise()(input_img)
    for layer in model.layers:
        layer.trainable = False
    fxprime = model(xprime)
    adv_model = Model(input_img, [xprime, fxprime])
    print(adv_model.summary())

    input_img_data, _ = load_images([input_img_path], image_size=img_size)
    input_img_data = np.array(input_img_data)
    losses = {"add_noise_1":'mae',
              "model_1":"binary_crossentropy"}
    loss_weights = {"add_noise_1":0.01, "model_1":1}
 
    #adv_model.compile(optimizer='adadelta', loss=losses, loss_weights=loss_weights)
    adv_model.compile(optimizer='rmsprop', loss=losses, loss_weights=loss_weights)
 
    if not os.path.exists('ckpt/'): os.makedirs('ckpt/')

    checkpoint = ModelCheckpoint('ckpt/adversarial_net.h5', verbose=1, monitor='loss', save_best_only=True, mode='auto')  

    adv_model.fit(input_img_data, {'add_noise_1':input_img_data, 'model_1': np.array([y_prime])},
                    epochs=2000,
                    batch_size=1,
                    shuffle=False,
                    callbacks=[TensorBoard(log_dir='/tmp/autoencoder'), checkpoint])
 
    #adv_model.save("ckpt/adversarial_net.h5")
    saved_model = keras.models.load_model('ckpt/adversarial_net.h5', custom_objects={'AddNoise': AddNoise})

    return (input_img_data[0], model.predict(input_img_data)), saved_model.predict(input_img_data)




if __name__ == '__main__':
    pretrained_classifier = keras.models.load_model("nsfw.299x299.h5")
    (out, fx), (xp, fxp) = adversarial_net(pretrained_classifier, "figs/playboy.png")
    categories = ['drawings', 'hentai', 'neutral', 'porn', 'sexy']
    for i, kind in enumerate(categories):
        print("P(%s|x) ="%kind, fx[0][i])
        print("P(%s|x') ="%kind, fxp[0][i])
    cv2.imwrite("figs/old_img.png", cv2.cvtColor(out*255, cv2.COLOR_RGB2BGR))
    cv2.imwrite("figs/new_img.png", cv2.cvtColor(xp[0]*255, cv2.COLOR_RGB2BGR))
